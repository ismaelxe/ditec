Arrays

push--> añade al final
unshift --> añade al principio

pop --> saca del final
shift --> saca del principio

slice(indice) --> parte el array
splice(indice, numero de items a borrar) --> elimina elementos a partir de un indice
remove(indice) --> elimina un item del array
indexOf(item) --> devuelve el indice del item
length --> longitud del array


//for
array = [1,2,3];

//for viejo ES5
for(var i = 0; i<array.length; i++){}

//forEach
array.forEach((intem, index)=>{});

for(let [item, index] of array){ //Al poner los corchetes podemos obtener los dos valores

}


sort --> sirve para ordenar (0-> igual, 1->mayor, -1 -> menor)

filter(item->item%2===0) // filtraria los numeros pares

map
[0,1,2].map(item=>({id:item}))

reduce --> seria el equivalente en SQL a sum, count, avg...
[0,1,2].reduce((a,b)=>{a + b},0)  //El 0 es el valor inicial


var map = new Map();
var map2 = new Map([[0, 'hola'], [1, 'adios']])

map.set(key, value)
map.delete(key)
map.get(key)
map.has(key)




